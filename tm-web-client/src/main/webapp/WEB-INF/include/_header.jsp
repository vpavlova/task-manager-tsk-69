<html>
<head>
    <title>${view_name}</title>
    <style>
        td {
            padding: 5px;
            border: navy dashed 1px;
        }

        th {
            font-weight: 700;
            text-align: left;
            background: cornflowerblue;
            border: black solid 1px;
            padding: 10px;
        }
    </style>
</head>
<body>
<h1>TASK MANAGER</h1>
<list class="left">
    <li><a href="/tasks">Tasks</a></li>
    <li><a href="/projects">Projects</a></li>
</list>
<div class="right">
    <sec:authorize access="isAuthenticated()">
        <a href="/logout">Logout</a>
    </sec:authorize>
    <sec:authorize access="!isAuthenticated()">
        <a href="/login">Login</a>
    </sec:authorize>
</div>
