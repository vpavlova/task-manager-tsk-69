package ru.vpavlova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vpavlova.tm.api.service.IProjectService;
import ru.vpavlova.tm.api.service.ITaskService;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.model.AuthorizedUser;
import ru.vpavlova.tm.model.Project;
import ru.vpavlova.tm.model.Task;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER"})
    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user
    ) {
        taskService.create(user.getUserId());
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "task-edit",
                "command", taskService.findById(user.getUserId(), id)
        );
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @ModelAttribute("task") Task task
    ) {
        taskService.add(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "TASK EDIT";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }

}