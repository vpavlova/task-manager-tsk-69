package ru.vpavlova.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractListener;

@Component
public class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application version.";
    }

    @Override
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("version"));
    }

}
