package ru.vpavlova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vpavlova.tm.api.repository.IRepository;
import ru.vpavlova.tm.dto.Project;

import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId);

    Optional<Project> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    @NotNull
    Optional<Project> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

}

